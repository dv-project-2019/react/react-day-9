import React, { useState, useEffect } from 'react';
import { Descriptions } from 'antd';
import { List, Typography } from 'antd';
import { Select } from 'antd';
import { Button } from 'antd';
import { Layout, Menu } from 'antd';
import { PageHeader } from 'antd';
import './todo-style.css';


const TodoPage = (props) => {
    const { Header, Content, Footer } = Layout;
    const { Option } = Select;
    const [user, setUser] = useState({});
    const [todoList, setTodoList] = useState([]);
    const [todoStatus, setTodoStatus] = useState([]);
    const [selectValue, setSelectValue] = useState('All');

    useEffect(() => {
        fetchUserDataById();
        fetchTodoData();
    }, [selectValue]);

    const fetchUserDataById = () => {
        //get user id
        const userId = props.match.params.user_id;

        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }

    const fetchTodoData = () => {
        const userId = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodoList(data);
                checkTodoStatus(data);
            })
            .catch(error => console.log(error));
    }

    const checkTodoStatus = (todoList) => {
        var statusList = [];
        todoList.map((todo) => {
            if (!statusList.includes(todo.completed.toString())) {
                statusList.push(todo.completed.toString());
            }
        })
        setTodoStatus(statusList);
    }

    const selectTodoStatus = (value) => {
        setSelectValue(value);
    }

    const clickDone = (id) => {
        var todoUpdate = [...todoList];
        todoUpdate.map((todo) => {
            if(todo.id == id){
                console.log(todo.id)
                todo.completed = true;
            }
        })
        setTodoList(todoUpdate);
    }

    return (
        <Layout className="layout">
            <Header>
                <h2 className="todolist-text">Todo List</h2>
            </Header>

            <Content className="content">
                <div className="main-todo-page">
                    <div className="user-info">
                        <Descriptions title={"User : " + user.name} bordered>
                            <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                            <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                            <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                            <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                        </Descriptions>
                    </div>

                    <List
                        header={
                            <div>
                                <p></p><b>Todo List</b>
                                <div className="select-box">
                                    <Select value={selectValue} onChange={selectTodoStatus} className="select">
                                        <Option value="All">All</Option>
                                        {todoStatus.map((todo, index) => {
                                            if (todo == "false") {
                                                return (
                                                    <Option key={index} value={todo}>Doing</Option>
                                                )
                                            } else if (todo == "true") {
                                                return (
                                                    <Option key={index} value={todo}>Done</Option>
                                                )
                                            }
                                        })
                                        }
                                    </Select>
                                </div>
                            </div>
                        }

                        bordered
                        dataSource={
                            selectValue == 'All' ?
                                todoList
                                :
                                todoList.filter((list) => list.completed.toString() == selectValue)
                        }
                        renderItem={(item, index) => (
                            <List.Item>
                                {item.completed == true ?
                                    <div>
                                        <s> Done </s>
                                        {item.title}
                                    </div>

                                    :
                                    <div>
                                        <Typography.Text mark> Doing </Typography.Text>
                                        {item.title}
                                    </div>
                                }
                                {item.completed == true ?
                                    <p></p>
                                    :
                                    <Button onClick={() => clickDone(item.id)} value={index} >Done</Button>
                                }
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}

export default TodoPage;