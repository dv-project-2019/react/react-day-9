import React, { useState, useEffect } from 'react';
import { Input } from 'antd';
import { List, Avatar, Skeleton } from 'antd';
import { Layout } from 'antd';
import './userpage-style.css';

const { Search } = Input;
const { Header, Content, Footer } = Layout;

const UserPage = () => {
    const [users, setUsers] = useState([]);
    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        fetchAllUser();
        console.log('useEffect');
    }, []);

    const fetchAllUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                setUsers(data);
            })
            .catch(error => console.log(error));
    }

    const searchUserByName = (event) => {
        setSearchValue(event.target.value);
    }

    return (
        <Layout className="layout">
            <Header>
                <h2 className="userlist-text">Users List</h2>
            </Header>
            <Content class="content">
                <div style={{ margin: '16px 0' }}>
                    <Search
                        placeholder="input user name"
                        onChange={searchUserByName}
                        style={{ width: 300, height: 40 }}
                    />
                </div>
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                    <List
                        className="demo-loadmore-list"
                        itemLayout="horizontal"
                        dataSource={
                            searchValue ?
                                users.filter(user => user.name.match(searchValue))
                                :
                                users
                        }
                        renderItem={item => (
                            <List.Item
                                actions={[<a href={"/users/" + item.id + "/todo"} >Todo</a>,
                                <a href={"/users/" + item.id + "/albums"}>Albums</a>]}
                            >
                                <Skeleton avatar title={false} loading={item.loading} active>
                                    <List.Item.Meta
                                        avatar={
                                            <Avatar src="https://www.bsglobaltrade.com/wp-content/uploads/2016/09/photo.png" />
                                        }
                                        title={<a href="https://ant.design">{item.name}</a>}
                                        description={item.email}
                                    />
                                </Skeleton>
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    );
}

export default UserPage;