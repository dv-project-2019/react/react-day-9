import React from 'react';
import UserPage from './pages/users/UserPage';
import TodoPage from './pages/todo/TodoPage';
import AlbumPage from './pages/albums/AlbumPage';
import PicturePage from './pages/picture/PicturePage';
import { BrowserRouter, Route } from 'react-router-dom';

const Routing = () => {
    return (
        <BrowserRouter>
            <Route path="/" component={UserPage} exact={true} />
            <Route path="/users" component={UserPage} exact={true} />
            <Route path="/users/:user_id/todo" component={TodoPage} />
            <Route path="/users/:user_id/albums" component={AlbumPage} exact={true} />
            <Route path="/users/:user_id/albums/:album_id" component={PicturePage} />
        </BrowserRouter>
    )
}

export default Routing;